<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

//use SebastianBergmann\Diff\Exception;

class UserController extends Controller
{
    public function login(Request $request)
    {
        try {
            $request->validate([  //login
                'email' => ['required', 'email'],
                'password' => ['required']
            ]);

            $credentials = request(['email', 'password']);
            if (!Auth::attempt($credentials)) {
                return array(
                    'Unauthorized' => '500',
                );
            }
            $user = User::where('email', $request->email)->first();
            if (!Hash::check($request->password, $user->password, [])) {
                throw new \Exception('Invalid Credentials');
            }
            $tokenResult = $user->createToken('authToken')->plainTextToken;
            $response=[
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'user' => $user,
                ];
            return response($response, 200);

        } catch (Exception $error) {

            return array(
                'message' => 'wrong',
                'error' => $error,
            );
        }

    }
    public function show(User $user){
        try {
            $user = Auth::user($user);
            return $user;
        } catch (Exception $error){
            return array(
                'message'=>'Wrong',
                'error'=>$error
            );
        }
    }

    public function store(Request $request)
    {
        try {
            $request->validate([  //Register
                'name' => ['required', 'string', 'max:100'],
                'email' => ['required', 'string', 'email', 'max:50', 'unique:users'],
                'password' => ['required', 'min:5'],
            ]);
            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);  //insert data
            $user = User::where('email', $request->email)->first();
            $tokenResult = $user->createToken('authToken')->plainTextToken;
            $data = [
                'access_token' => $tokenResult,
                'token_type' => 'Bearer',
                'user' => $user,
            ];
//        return $this->sendResponse($data, 'Successfull Register');
            return $data;
        } catch (Exception $error) {

            return array(
                'message' => 'Sometfing went wrong',
                'error' => $error,
            );
        }
    }

    public function logout()
    {
        $user = User::find(Auth::user()->id);

        $user->tokens()->delete();
        return response()->noContent();
    }

}
