<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\order_product;
use App\Models\product;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\order;
use Illuminate\Support\Facades\Auth;
use function Symfony\Component\HttpKernel\Log\format;

class OrderController extends Controller
{
    public function store(Request $request)
    {
        $orders = is_array($request->product_id) ? $request->product_id : [];
        foreach ($orders as $key => $quantity) {
            $price = product::where('id', $key)->select('price')->first();
            $total = $price['price'] * $quantity;
        }
//        $email=User::where('id',Auth()->id())->select('email')->first();
        $order_id = Order::create([
            'date' => $request->date,
            'phone' => $request->phone,
            'email' => $request->email,
            'address' => $request->address,
            'total_price' => $total,

        ]);
        foreach ($orders as $key => $quantity) {
            order_product::create([
                'product_id' => $key,
                'order_id' => $order_id->id,
            ]);
        }
        return array(
            'success' => '1',
        );
    }
    public function search($date){
        return order::where('date','like','%'.$date.'%')->get();
    }
    public function update(Request $request, $id){
        $order=order::find($id);
        $order->update($request->all());
        return $order;
    }

    public function delete($id){
        return order::destroy($id);
    }

}
