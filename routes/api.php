<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\OrderController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::middleware('auth:sanctum')->group(function(){

    Route::get('user',[UserController::class,'show']);
    Route::get('user/logout', [UserController::class, 'logout']);

    Route::post('order/store',[OrderController::class,'store']);
    Route::put('order/update/{id}',[OrderController::class,'update']);
    Route::delete('order/delete/{id}',[OrderController::class,'delete']);
    Route::get('order/search/{date}',[OrderController::class,'search']);


});

Route::post('users', [UserController::class, 'store']);
Route::post('user/login', [UserController::class, 'login']);





